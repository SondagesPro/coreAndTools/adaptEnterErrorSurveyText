<?php

/**
 *Plugin for limesurvey : Use your own string and text when user try to enter a survey : not started / expired / bad token.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2016-2023 Denis Chenu <http://www.sondages.pro>
 * @copyright 2016-2023 Advantages <http://www.advantages.pro>
 * @license AGPL v3
 * @version 0.2.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class adaptEnterErrorSurveyText extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Use your own string and text when user trye to enter a survey : not started, expired or bad token.';
    protected static $name = 'adaptEnterErrorSurveyText';

  /**
   * The settings for this plugin
   * @Todo Move this to the constructor to add all language of system
   * @Todo Add the script for ckeditor
   */
    protected $settings = array(
    "useCompletedTemplate" => array(
      'type' => 'checkbox',
      'label' => 'Use completed.pstpl template file to render the text',
      'help' => 'If you not activate this : the message is render with %s.',
      'htmlOptions' => array(
        'uncheckValue' => '',
      ),
      'default' => '1'
    ),
    );

  /**
   * @var array $errorTexts The array of settings key with their english label n(for translation
   * @todo : us it in contructor to contruct $this->settings
   */
    private $errorTexts = array(
        'errorTextNotStarted' => 'Text to be shown when a respondant try to enter a not started survey',
        'errorTextExpired' => 'Text to be shown when a respondant try to enter an expired survey',
        'errorTextUsedToken' => 'Text to be shown when a respondant try to enter a survey with an already used token',
    );

  /**
   * @var array[] $translation The array of translation string
   */
    private $translation = array(
        'Text to be shown when a respondant try to enter a not started survey' => array('fr' => "Texte à montrer quand le questionnaire n'est pas commencé"),
        'Text to be shown when a respondant try to enter an expired survey' => array('fr' => "Texte à montrer quand le questionnaire est terminé"),
        'Text to be shown when a respondant try to enter a unexisting survey' => array('fr' => "Texte à montrer si le questionnaire n'exite pas ou plus"),
        'Text to be shown when a respondant try to enter a survey with an already used token' => array('fr' => "Texte à montrer si le code d'invitation est déjà utilisé"),
        'Text to be shown when a respondant try to enter a survey with a bad token' => array('fr' => "Texte à montrer en cas de mauvaise saisie du jeton"),
        'Title' => array('fr' => "Titre"),
        'Message' => array('fr' => "Message"),
        'HTML text to be shown in %s.' => array('fr' => "Code HTML à utiliser pour la langue %s."),
        'Use completed.pstpl template file to render the text' => array('fr' => "UItiliser le fichier completed.pstpl pour le rendu du texte"),
        'If you not activate this : the message is render with %s.' => array('fr' => "Si vous ne cochez pas cette case, le rendu utilisera ce code : %s."),
    );
    /* string[] translation to be used for placeholder and default text */
    private static $coreMessages = [
        'errorTextNotStarted' => [
            'title' => "Error",
            'message' => "This survey is not yet started."
        ],
        'errorTextExpired' => [
            'title' => "Error",
            'message' => "We are sorry but the survey is expired and no longer available."
        ],
        'errorTextUsedToken' => [
            'title' => "This invitation has already been used.",
            'message' => "We are sorry but you are not allowed to enter this survey."
        ],
    ];

  /**
   * @var array $language language to be used in public
   */
    private $language;

    public function init()
    {
        $this->subscribe('beforeSurveyPage');
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
    }

    public function beforeSurveySettings()
    {
        if (!$this->getEvent() || !Permission::model()->hasSurveyPermission($this->getEvent()->get('survey'), 'surveysettings', 'read')) {
            throw new CHttpException(403);
        }
        $oEvent = $this->event;
        $newSettings = array();
        $oSurvey = Survey::model()->findByPk($oEvent->get('survey'));
        $aLang = $oSurvey->getAllLanguages();

        $aCurrent = array();
        foreach ($this->errorTexts as $setting => $label) {
            $aCurrent[$setting] = $this->get($setting, 'Survey', $oEvent->get('survey'), array());
            if (version_compare(App()->getConfig("versionnumber"), "3", ">=")) {
                $aCurrent["title" . $setting] = $this->get("title" . $setting, 'Survey', $oEvent->get('survey'), array());
            }
        }
        $apiVersion = explode(".", App()->getConfig("versionnumber"));
        if ($apiVersion[0] >= 2 && $apiVersion[1] >= 50) {
            $cssUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/fix.css');
            Yii::app()->clientScript->registerCssFile($cssUrl);
        }
        if (version_compare(App()->getConfig("versionnumber"), "3", ">=")) {
            foreach ($aLang as $sLang) {
                $newSettings["title_$sLang"] = array(
                    'type' => 'info',
                    'content' => "<strong class='label label-info'>" . sprintf($this->_translate("HTML text to be shown in %s."), $sLang) . "</strong>",
                );
                foreach ($this->errorTexts as $setting => $label) {
                    $newSettings["info{$setting}_{$sLang}"] = array(
                        'type' => 'info',
                        'content' => "<strong>" . $this->_translate($label) . "</strong>",
                    );
                    $newSettings["title{$setting}_{$sLang}"] = array(
                        'type' => 'string',
                        'label' => $this->_translate("Title"),
                        'localized' => true,
                        'language' => $sLang,
                        'current' => $aCurrent["title" . $setting],
                        'htmlOptions' => array(
                            'placeholder' => gT(self::$coreMessages[$setting]['title'], 'unescaped', $sLang),
                        ),
                    );
                    $newSettings["{$setting}_{$sLang}"] = array(
                        'type' => 'html',
                        'label' => $this->_translate("Message"),
                        'htmlOptions' => array(
                            'class' => 'form-control',
                            'height' => '6em',
                            'placeholder' => gT(self::$coreMessages[$setting]['message'], 'unescaped', $sLang),
                        ),
                        'height' => '6em',
                        'editorOptions' => array(
                            "font-styles" => false,
                            "html" => true,
                            "link" => false, // broken in LS
                            "image" => false, // broken in LS
                            "lists" => false,
                        ),
                        'localized' => true,
                        'language' => $sLang,
                        'current' => $aCurrent[$setting],
                    );
                }
            }
        } else {
            foreach ($aLang as $sLang) {
                $newSettings["title_$sLang"] = array(
                'type' => 'info',
                'content' => "<strong class='label label-info'>" . sprintf($this->_translate("HTML text to be shown in %s."), $sLang) . "</strong>",
                );
                foreach ($this->errorTexts as $setting => $label) {
                    $newSettings["{$setting}_{$sLang}"] = array(
                        'type' => 'html',
                        'label' => $this->_translate($label),
                        'htmlOptions' => array(
                            'class' => 'form-control',
                            'height' => '6em',
                        ),
                        'height' => '6em',
                        'editorOptions' => array(
                            "font-styles" => false,
                            "html" => true,
                            "link" => false, // broken in LS
                            "image" => false, // broken in LS
                            "lists" => false,
                        ),
                        'localized' => true,
                        'language' => $sLang,
                        'current' => $aCurrent[$setting],
                    );
                }
            }
        }
        $oEvent->set("surveysettings.{$this->id}", array(
        'name' => get_class($this),
        'settings' => $newSettings
        ));
    }

    public function newSurveySettings()
    {
        if (!$this->getEvent() || !Permission::model()->hasSurveyPermission($this->getEvent()->get('survey'), 'surveysettings', 'update')) {
            throw new CHttpException(403);
        }
        $event = $this->event;
        $aLangSettings = array();
        foreach ($event->get('settings') as $name => $value) {
            if (strpos($name, "_")) {
                $aNameLang = explode("_", $name);
                $name = $aNameLang[0];
                if (isset($aNameLang[2])) {
                    $lang = "{$aNameLang[1]}_{$aNameLang[2]}";
                } else {
                    $lang = "{$aNameLang[1]}";
                }
                if (!isset($aLangSettings[$name])) {
                    $aLangSettings[$name] = array();
                }
                $aLangSettings[$name][$lang] = $value[$lang];
            } else {
              /* In order use survey setting, if not set, use global, if not set use default */
                $this->set($name, $value, 'Survey', $event->get('survey'));
            }
        }
        foreach ($aLangSettings as $setting => $aValues) {
            $this->set($setting, $aValues, 'Survey', $event->get('survey'));
        }
    }

    public function beforeSurveyPage()
    {
        $oEvent = $this->event;
        if (!$this->getEvent() || $this->getEvent()->getEventName() != 'beforeSurveyPage') {
            throw new CHttpException(403);
        }
        $iSurveyId = $oEvent->get('surveyId');
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        $this->setLanguage();
      /* We are sure the survey exist .... */
        if ($oSurvey && !in_array(Yii::app()->request->getQuery('action', ''), array('previewquestion','previewgroup'))) {
            if ($oSurvey->active == "Y") {
                $this->ThrowExpired();
                $this->ThrowNotStarted();
                $this->ThrowToken();
            }
        }
    }

    private function ThrowExpired()
    {
        $oSurvey = Survey::model()->findByPk($this->event->get('surveyId'));
        if ($oSurvey->expires && $oSurvey->expires < dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"))) {
            $this->renderError('errorTextExpired');
        }
    }
    private function ThrowNotStarted()
    {
        $oSurvey = Survey::model()->findByPk($this->event->get('surveyId'));
        if ($oSurvey->startdate && $oSurvey->startdate >= dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"))) {
            $this->renderError('errorTextNotStarted');
        }
    }
    private function ThrowToken()
    {
        $iSurveyId = $this->event->get('surveyId');
        $oSurvey = Survey::model()->findByPk($iSurveyId);
        if (tableExists("{{tokens_" . $iSurveyId . "}}")) {
            $token = Yii::app()->request->getParam('token', '');
            if ($token) {
                $oToken = Token::model($iSurveyId)->find("token=:token", array(':token' => $token));
                $now = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust"));
                if (!$oToken) {
                  // @todo
                } elseif ($oToken->completed != "N" && $oSurvey->alloweditaftercompletion != 'Y') {
                    $this->renderError('errorTextUsedToken');
                } elseif ($oToken->validfrom && strtotime($now) < strtotime($oToken->validfrom)) {
                    $this->renderError('errorTextNotStarted');
                } elseif ($oToken->validuntil && strtotime($now) > strtotime($oToken->validuntil)) {
                    $this->renderError('errorTextExpired');
                }
            }
        }
      //~ if($oSurvey->startdate && $oSurvey->startdate >= dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", Yii::app()->getConfig("timeadjust")))
      //~ {
      //~ $this->renderError('errorTextNotStarted');
      //~ }
    }
  /**
   * Set the language to be used
   * @return void
   */
    private function setLanguage()
    {
        $iSurveyId = $this->event->get('surveyId');
        $sLanguage = Yii::app()->request->getParam('lang', '');
        if (empty($sLanguage) && isset($_SESSION['survey_' . $iSurveyId]['s_lang'])) {
            $sLanguage = $_SESSION['survey_' . $iSurveyId]['s_lang'];
        }
        if (!in_array($sLanguage, Survey::model()->findByPk($iSurveyId)->getAllLanguages())) {
            $sLanguage = Survey::model()->findByPk($this->event->get('surveyId'))->language;
        }
        $this->language = $sLanguage;
    }
  /**
   * render the error to be shown
   * @param $setting : the setting to be used
   *
   * return @void
   */
    private function renderError($setting)
    {
        $iSurveyId = $this->event->get('surveyId');
        $aSurveyMessages = array_filter($this->get($setting, 'Survey', $this->event->get('surveyId'), array()));
        $aDefaultMessages = $this->get($setting, null, null, array());
        $aMessage = array_merge($aDefaultMessages, $aSurveyMessages);
        if (empty($aMessage[$this->language]) || !trim($aMessage[$this->language])) {
            return;
        }
        Yii::app()->setConfig('surveyID', $iSurveyId); // For templatereplace
        $oSurvey = Survey::model()->findByPk($this->event->get('surveyId'));
        // Set the language for templatereplace
        SetSurveyLanguage($iSurveyId, $this->language);
        $lsVersion = App()->getConfig("versionnumber");
        if (version_compare($lsVersion, "3", ">=")) {
            if (Yii::getPathOfAlias('renderMessage')) {
                $this->renderMessage300($iSurveyId, $setting);
            }
            $this->log("Unable to use adaptEnterErroSurveyText without renderMessage plugin", 'error');
        }
        if (version_compare($lsVersion, "2.50", "<=")) {
            $this->renderMessage250($aMessage[$this->language], $oSurvey->template);
        }
        if (version_compare($lsVersion, "2.6", "<=")) {
            $this->renderMessage206($aMessage[$this->language], $oSurvey->template);
        }

      /* 2.06 quick system (no render ...) */
    }

  /**
   * render a public message for 2.06 and lesser
   */
    private function renderMessage206($message, $template)
    {
        $templateDir = Template::getTemplatePath($template);
        $renderData['message'] = $message;
        App()->controller->layout = 'bare';
        $renderData['language'] = $this->language;
        if (getLanguageRTL($this->language)) {
            $renderData['dir'] = ' dir="rtl" ';
        } else {
            $renderData['dir'] = '';
        }
        $renderData['templateDir'] = $templateDir;
        $renderData['useCompletedTemplate'] = $this->get('useCompletedTemplate', null, null, $this->settings['useCompletedTemplate']['default']);
        Yii::setPathOfAlias('adaptEnterErrorSurveyText', dirname(__FILE__));
        App()->controller->render("adaptEnterErrorSurveyText.views.2_06.public", $renderData);
        App()->end();
    }
  /**
   * render a public message for 2.50 and up
   */
    private function renderMessage250($message, $template)
    {
        $oTemplate = Template::model()->getInstance($template);
        $templateDir = $oTemplate->viewPath;
        $renderData['message'] = $message;
        App()->controller->layout = 'bare';
        $renderData['language'] = $this->language;
        if (getLanguageRTL($this->language)) {
            $renderData['dir'] = ' dir="rtl" ';
        } else {
            $renderData['dir'] = '';
        }
        $renderData['templateDir'] = $templateDir;
        $renderData['useCompletedTemplate'] = $this->get('useCompletedTemplate', null, null, $this->settings['useCompletedTemplate']['default']);
        Yii::setPathOfAlias('adaptEnterErrorSurveyText', dirname(__FILE__));
        App()->controller->render("adaptEnterErrorSurveyText.views.2_50.public", $renderData);
        App()->end();
    }

  /**
   * render alert in 3.0 and up
   */
    private function renderMessage300($surveyid, $setting)
    {
        /* Rendering using error_layout */
        $language = App()->getLanguage();
        $aSurveyinfo = getSurveyInfo($surveyid, App()->getLanguage());
        Template::model()->getInstance('', $surveyid);
        $aMessages = $this->get($setting, 'Survey', $surveyid, array());
        $message = gT(self::$coreMessages[$setting]['message']);
        if (!empty($aMessages[$language])) {
            $message = $aMessages[$language];
        }
        $aTitles = $this->get("title" . $setting, 'Survey', $surveyid, array());
        $title = gT(self::$coreMessages[$setting]['title']);
        if (!empty($aTitles[$language])) {
            $title = $aTitles[$language];
        }
        $aError = array(
            'title' => $title, // In the title ?
            'message' => $message,
        );
        $aRenderData = array(
            'aError' => $aError,
            'aSurveyInfo' => array(
                'aError' => $aError,
            )
        );
        $messageHelper = new \renderMessage\messageHelper();
        $messageHelper->render('', 'errors', '', $aRenderData);
    }
  /**
   * Add the settings by lang
   * The core system is broken or hard to use : seems we can not have same setting with different language (the name is the key)
   */
    public function getPluginSettings($getValues = true)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'read')) {
            throw new CHttpException(403);
        }
        $this->settings = parent::getPluginSettings($getValues);
        $restrictedToLanguage = trim(Yii::app()->getConfig('restrictToLanguages'));
        if (empty($restrictedToLanguage)) {
            $aLang = array(Yii::app()->getConfig('defaultlang'));
        } else {
            $aLang = explode(' ', $restrictedToLanguage);
        }
        $this->settings["useCompletedTemplate"]["label"] = $this->_translate($this->settings["useCompletedTemplate"]["label"]);
        $this->settings["useCompletedTemplate"]["help"] = sprintf($this->_translate($this->settings["useCompletedTemplate"]["help"]), '<code>&lt;div id="wrapper"&gt;&lt;p id="token"&gt Your message &lt;p&gt;&lt;div&gt;</code>');

        $apiVersion = explode(".", App()->getConfig("versionnumber"));

        if ($apiVersion[0] >= 2 && $apiVersion[1] >= 50) {
            $cssUrl = Yii::app()->assetManager->publish(dirname(__FILE__) . '/assets/fix.css');
            Yii::app()->clientScript->registerCssFile($cssUrl);
        }

        foreach ($aLang as $sLang) {
            $this->settings["title_{$sLang}"] = array(
            'type' => 'info',
            'content' => "<strong class='label label-info'>" . sprintf($this->_translate("HTML text to be shown in %s."), $sLang) . "</strong>",
            );

            foreach ($this->errorTexts as $setting => $label) {
                $this->settings["{$setting}_{$sLang}"] = array(
                // 'name'=>$setting, see bug report : https://bugs.limesurvey.org/view.php?id=11666
                'type' => 'html',
                'label' => $this->_translate($label),
                'height' => '10em',
                'htmlOptions' => array(
                'class' => 'form-control',
                'height' => '15em',
                ),
                'editorOptions' => array(
                "font-styles" => false,
                "html" => true,
                "link" => false, // broken in LS
                "image" => false, // broken in LS
                ),
                'localized' => true,
                'language' => $sLang,
                );
            }
            if ($getValues) {
                foreach ($this->errorTexts as $setting => $label) {
                    $this->settings["{$setting}_{$sLang}"]['current'] = $this->get($setting, null, null, array());
                }
            }
        }

        return $this->settings;
    }
  /**
   * Fix language settings
   */
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        $aFixedSettings = array();
        foreach ($settings as $name => $value) {
            if (strpos($name, "_")) {
                $aNameLang = explode("_", $name);
                if (!isset($aFixedSettings[$aNameLang[0]])) {
                    $aFixedSettings[$aNameLang[0]] = array();
                }
                if (isset($aNameLang[2])) {
                    $lang = "{$aNameLang[1]}-{$aNameLang[2]}";
                } else {
                    $lang = "{$aNameLang[1]}";
                }
                $aFixedSettings[$aNameLang[0]][$lang] = $value[$lang];
            } else {
                $aFixedSettings[$name] = $value;
            }
        }
        parent::saveSettings($aFixedSettings);
    }

  /**
   * quick language system
   */
    private function _translate($string)
    {
        if (isset($this->translation[$string][Yii::app()->language])) {
            return $this->translation[$string][Yii::app()->language];
        }
        return $string;
    }
}
